'use strict';


$('#ign_ank').on('click', function(){
  $.scrollTo( '#ign_point', 600, {easing:'easeInOutSine'});
});

$('#askmen_ank').on('click', function(){
  $.scrollTo( '#askmen_point', 600, {easing:'easeInOutSine'});
});

$('.top').on('click', function(){
  $.scrollTo( '#ignHeader', 800, {easing:'easeInOutSine'});
});


/**all js that isn't linked up to directives**/
// behavior on download buttons
$(document).on('click', 'a.button', function(){

  var origiText = $(this).find('span.text').text();

  if($(this).hasClass('activeLoad') !== true) {

    $(this).find('span.text').animate({

        marginTop : '50px'

    }, 600, 'swing', function(){

      $(this).hide().css('margin-top', '0px').text('DOWNLOADING').fadeIn(300).parent().addClass('activeLoad');

        var self = this;

        setTimeout(function(){

            $(self).fadeOut(300, function(){

              $(self).text(origiText).fadeIn(300).parent().removeClass('activeLoad');

            });

        }, 1200);

    });

    // var linkHref = $(this).attr('href');
    // $.fileDownload(linkHref);
    // return false; //this is critical to stop the click event which will trigger a normal file download!

  }

});



// stop default behavior on links
$('#view').on('click', 'a', function(event){
    event.preventDefault();
});


// add background to menu items
$('#menu').find('li').on('click', function(){

  if($(this).hasClass('active') !== true) {
      $('#menu').find('li').removeClass('active');
      $(this).addClass('active');
  } 

});

// trigger modal on download button
$(document).ready(function(){
	$(".circle").live('click', function(e) {
    e.stopPropagation();
	  $("#secondModal").reveal();
	  console.log('Yeah its hooked up...');
	});

  $(".logo_container").live('click', function() {
    $("#myModal").reveal({
      "open": function(){
        $(this).find('img').hide().fadeIn(600);
      },
      "closed": function () {
        $(this).find('img').fadeOut(300).attr('src', '');
      } 
    });

  });

});


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).value('$anchorScroll', angular.noop).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/ign', {templateUrl: 'partials/partial1.html', controller: MyCtrl1});
    $routeProvider.when('/askmen', {templateUrl: 'partials/partial2.html', controller: MyCtrl2});
    $routeProvider.otherwise({redirectTo: '/ign'});
  }]).directive('coolFade', function() {
    return {
      compile: function(elm) {
        console.log('compiling');
        $(elm).css('opacity', 0.1);
        return function(scope, elm, attrs) {
          console.log('animating');
          $(elm).animate({ opacity : 1.0 }, 1000 );

          $(document).ready(function() {
        
            $('.iosSlider').iosSlider({
              snapToChildren: true,
              desktopClickDrag: true,
              infiniteSlider: true,
              snapSlideCenter: true,
              onSlideChange: slideChange,
              stageCSS: {
                overflow: 'visible'
              }


            });

            // $('.logo_container').on('mouseenter', function(){
            //     $(this).find('.format_container').stop().fadeIn(200);
            // });

            // $('.logo_container').on('mouseleave', function(){
            //     $(this).find('.format_container').stop().fadeOut(1000);
            // });

            $('.logo_container').on('click', function(){

              var x = $(this).find('.download_btn').attr('data-number');

                $.getJSON('http://uk-microsites.ign.com/brand/json_data/logos.json', function(data) {

                //console.log(data.logos[x].logoTitle);

                    $('#myModal').find('h3').text(data.logos[x].logoTitle); //get title

                    $('#myModal').find('img').attr('src', data.logos[x].imageFile); //get image

                    console.log('Image file --> ' + x + data.logos[x].imageFile);

                    console.log('modal color --> ' + x + data.logos[x].modalColor);

                    if($('#myModal').hasClass('white') ) {
                        $('#myModal').removeClass('white');
                        $('#myModal').addClass(data.logos[x].modalColor);
                    } else {
                        $('#myModal').removeClass('black');
                        $('#myModal').addClass(data.logos[x].modalColor);
                    }

                    var formatCont = $('#myModal').find('.buttons_container'); //set format container variable

                    $(formatCont).empty(); //empty format container before filling it

                    $.each(data.logos, function(i, data){
                      console.log(data.formats);

                        if( i == x ) {

                          console.log(i);
                          $.each(data.formats, function(key, value){
                                console.log(key, value);
                                var div_data ="<li><a class='button' href=''><span class='rotate'><div class='arrow-down'>"+value+"</div></span>"+key+"</a></li>";
                                $(div_data).appendTo(formatCont);
                          });

                          $.each(data.url, function(key, value){
                                console.log(key, value);
                                //var div_data ="<li><a class='button' href='"+data.url_+key+"'><span class='rotate'><div class='arrow-down'>"+value+"</div></span>"+key+"</a></li>";
                                $(formatCont).find("a:contains("+ key +")").attr('href', value);
                          });

                        }

                      
                    });
                    
              });


            })
            
          }); 
          
          function slideChange(args) {
          
            try {
              console.log('changed: ' + (args.currentSlideNumber - 1));
            } catch(err) {
            }
          
          }
        };
      }
    };
  });//.directive( "preloader", PreloaderFactory );


  function MainCtrl($scope, $location) {

      $scope.setRoute = function(route) {
          $location.path(route);
      }

  }

//   PreloaderFactory = function( $compile ) {

//   var directive = {

//     restrict : "A",

//     link : function( scope, elm, attrs ) {

//       scope.preloader = jQuery( "<ng-include />" );
//       scope.preloader.attr( "src", "'"+ attrs.preloader +".html'" );

//       $compile( scope.preloader )( scope );

//       jQuery( "body" ).append( scope.preloader );

//       scope.$on( "applicationComplete", function( event ) {

//         jQuery( scope.preloader ).remove();
//       });
//     }
//   }
//   return directive;
// }

// function PreloaderController( $scope, $timeout ) {

//   $scope.percentCompleted = 0;

//   $scope.applicationProgressMock = function() {

//     if( $scope.percentCompleted == 100 ) {
//       $scope.$emit( "applicationComplete" );
//     }
//     else
//     {
//       $scope.percentCompleted += 5;
//       $timeout( $scope.applicationProgressMock, 100 );
//     }
//   }
//   $timeout( $scope.applicationProgressMock, 100 );

//   $scope.clickHandler = function( event ) {
//     $scope.percentCompleted = 100;
//   }
// }



